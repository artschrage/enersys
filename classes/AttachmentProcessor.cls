public with sharing class AttachmentProcessor {
	
	private Boolean triggerIsInsert;
	private Boolean triggerIsUpdate;
	private Boolean triggerIsDelete;
	private Boolean triggerIsUndelete;
	
	private Map<Id, Attachment> triggerOldMap;
	private Map<Id, Attachment> triggerNewMap;
	
    public AttachmentProcessor(Boolean isInsert, Boolean isUpdate, Boolean isDelete, Boolean isUndelete, Map<Id, Attachment> oldMap, Map<Id, Attachment> newMap) {
    	triggerIsInsert = isInsert;
		triggerIsUpdate = isUpdate;
		triggerIsDelete = isDelete;
		triggerIsUndelete = isUndelete;
		triggerOldMap = oldMap;
		triggerNewMap = newMap;
    }
    
    public void updateWSRequests() {
    	if(triggerIsInsert) {
	    	List<WS_Request__c> wsRequestsToUpdate = new List<WS_Request__c>();
	    	Set<Id> wsRequestIds = new Set<Id>();
	    	for(Attachment att : triggerNewMap.values()){	    		
	    		String keyPrefix = String.valueOf(att.ParentId).substring(0,3);
	    		
	    		if(keyPrefix == WS_Request__c.sObjectType.getDescribe().getKeyPrefix()) {
	    			wsRequestIds.add(att.ParentId);
	    		}
			}
			
			if(!wsRequestIds.isEmpty()) {
				wsRequestsToUpdate = [ SELECT Payload_in_Attachment__c FROM WS_Request__c WHERE Id = :wsRequestIds ];
				for(WS_Request__c req : wsRequestsToUpdate) {
					req.Payload_in_Attachment__c = true;
				}
				
				update wsRequestsToUpdate;
			}
    	}
    }
}