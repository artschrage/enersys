public with sharing class AttachmentTriggerHandler {
    public static void processAfter(Boolean isInsert, Boolean isUpdate, Boolean isDelete, Boolean isUndelete, Map<Id, Attachment> oldMap, Map<Id, Attachment> newMap) {
		AttachmentProcessor processor = new AttachmentProcessor(isInsert, isUpdate, isDelete, isUndelete, oldMap, newMap);
		processor.updateWSRequests();
	}
}