public without sharing class CPQEMEALineItemsController {

    public String lang { get; private set; }
    public SAPQuoteXmlParser.SAPQuoteDocument quoteDocument { get; private set; }
	
    public CPQEMEALineItemsController() {
        Id qId = (Id)ApexPages.currentPage().getParameters().get('qid');
        lang = (String) ApexPages.currentPage().getParameters().get('lang');
        
        quoteDocument = new SAPQuoteXmlParser.SAPQuoteDocument();
        selectQuote(qId);
    }

    @TestVisible  
    private CPQEMEALineItemsController(Id qId) {
    	quoteDocument = new SAPQuoteXmlParser.SAPQuoteDocument();
        selectQuote(qId);
    }
	
	private void selectQuote(Id quoteId) {
		List<Attachment> att = [ SELECT Body FROM Attachment WHERE ParentId = :quoteId ];
		if(att.size() > 0 && att[0].Body != null && !String.isBlank(att[0].Body.toString())) {
			SAPQuoteXmlParser parser = new SAPQuoteXmlParser(att[0].Body.toString());
			quoteDocument = parser.parse();
		}	
	}
}