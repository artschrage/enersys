public with sharing class QATestDataFactory {

	private static final String MOTIVE_TEST_QUOTE_NAME = 'Q-00241';
	private static final String MOTIVE_PRICEBOOK_NAME = 'Motive Power - Americas 701';
	private static final String RESERVE_TEST_QUOTE_NAME = 'Q-00146';
	private static final String RESERVE_PRICEBOOK_NAME = 'Reserve Power - Americas 701';

	String quoteType;
	Set<String> partNumbers;
	Set<Id> validProductIds;
	Set<Id> validProductIdsWithPrice;
	Map<String, Product2> mapPartNumberToProduct;
	Id pricebookId;

	public QATestDataFactory() {
		partNumbers = new Set<String>();
		validProductIds = new Set<Id>();
		validProductIdsWithPrice = new Set<Id>();
		mapPartNumberToProduct = new Map<String, Product2>{};

	}

	public void validateMotiveData() {
		initMotive();
		checkForProductsInSF();
		checkForPricesInSF();

	}

	public void validateReserveData() {
		initReserve();
		checkForProductsInSF();
		checkForPricesInSF();

	}

	public void resetMotiveData() {
		initMotive();
		checkForProductsInSF();

	}

	public void runTestMotive01() {
		initMotive();
		insertTestWSRequest(createJsonTestPayload(createJsonTestMotive01QuoteConfigs()));

	}

	public void runTestMotive02() {
		initMotive();
		insertTestWSRequest(createJsonTestPayload(createJsonTestMotive02QuoteConfigs()));

	}

	public void runTestReserve01() {
		initReserve();
		insertTestWSRequest(createJsonTestPayload(createJsonTestReserve01QuoteConfigs()));

	}

	private void initMotive() {
		quoteType = 'motive';
		createMotivePartNumbersSet();
		getMotivePricebookId();

	}

	private void initReserve() {
		quoteType = 'reserve';
		createReservePartNumbersSet();
		getReservePricebookId();

	}

	private void createMotivePartNumbersSet() {
		partNumbers.add('66211G-WBKC');
		partNumbers.add('66211-WBKC');
		partNumbers.add('503744S-WGRN');
		partNumbers.add('503744D-WGRN');
		partNumbers.add('EI1-CM-2GE');
		partNumbers.add('EI1-HL-2YE');
		partNumbers.add('EI3-HL-4YE');
		partNumbers.add('EI3-IN-4CE');
		partNumbers.add('EI3-IN-4YE');
		partNumbers.add('EI3-LP-4GE');
		partNumbers.add('EI3-NR-4GE');

	}

	private void createReservePartNumbersSet() {
		partNumbers.add('2DX-31');
		partNumbers.add('UD2H2T216A');
		partNumbers.add('UD2H3T180A');
		partNumbers.add('827511');
		partNumbers.add('881867');

	}

	private void insertTestWSRequest(ConfiguratorWS.QuoteConnectorPayload payload) {
		insert new WS_Request__c(JSON_Payload__c=JSON.serializePretty(payload));
	}

	private ConfiguratorWS.QuoteConnectorPayload createJsonTestPayload(List<ConfiguratorWS.QuoteConfig> quoteConfigs) {
		ConfiguratorWS.QuoteConnectorPayload payload = new ConfiguratorWS.QuoteConnectorPayload();
		payload.quoteConfigs = quoteConfigs;
		return payload;
	}

	private List<ConfiguratorWS.QuoteConfig> createJsonTestMotive01QuoteConfigs() {
		List<ConfiguratorWS.QuoteConfig> returnList = new List<ConfiguratorWS.QuoteConfig>{};

		ConfiguratorWS.QuoteConfig newQuote = new ConfiguratorWS.QuoteConfig();
		newQuote.configuratorName = 'OEMSpec';
		newQuote.sfQuoteNumber = MOTIVE_TEST_QUOTE_NAME;
		newQuote.truckSpecs = createJsonTestMotive01Truck1Specs();
		newQuote.lineItems = createJsonTestMotive01Truck1Battery1LineItems();
		returnList.add(newQuote);

		newQuote = new ConfiguratorWS.QuoteConfig();
		newQuote.configuratorName = 'OEMSpec';
		newQuote.sfQuoteNumber = MOTIVE_TEST_QUOTE_NAME;
		newQuote.truckSpecs = createJsonTestMotive01Truck1Specs();
		newQuote.lineItems = createJsonTestMotive01Truck1Battery2LineItems();
		returnList.add(newQuote);

		newQuote = new ConfiguratorWS.QuoteConfig();
		newQuote.configuratorName = 'OEMSpec';
		newQuote.sfQuoteNumber = MOTIVE_TEST_QUOTE_NAME;
		newQuote.truckSpecs = createJsonTestMotive01Truck2Specs();
		newQuote.lineItems = createJsonTestMotive01Truck2Battery1LineItems();
		returnList.add(newQuote);

		newQuote = new ConfiguratorWS.QuoteConfig();
		newQuote.configuratorName = 'OEMSpec';
		newQuote.sfQuoteNumber = MOTIVE_TEST_QUOTE_NAME;
		newQuote.truckSpecs = createJsonTestMotive01Truck2Specs();
		newQuote.lineItems = createJsonTestMotive01Truck2Battery2LineItems();
		returnList.add(newQuote);

		return returnList;
	}

	private List<ConfiguratorWS.QuoteLineItem> createJsonTestMotive01Truck1Battery1LineItems() {
		List<ConfiguratorWS.QuoteLineItem> returnItems = new List<ConfiguratorWS.QuoteLineItem>{};

		returnItems.add(createJsonLineItem('66211G-WBKC', 1, null, '66211G-WBKC-12-85G-13', false, false, null, null, null, null));
		returnItems.add(createJsonLineItem('EI1-CM-2GE', 1, null, 'EI1-CM-2GE-208V', false, false, null, null, '208', '24V'));
		returnItems.add(createJsonLineItem('EI1-CM-2GE', 1, null, 'EI1-CM-2GE-220V', false, false, null, null, '220', '24V'));
		returnItems.add(createJsonLineItem('EI1-HL-2YE', 1, null, 'EI1-HL-2YE-480V', false, false, null, null, '480', '24V'));
		returnItems.add(createJsonLineItem('EI3-HL-4YE', 1, null, 'EI3-HL-4YE-480V', false, true, null, null, '480', '24V'));

		return returnItems;
	}

	private List<ConfiguratorWS.QuoteLineItem> createJsonTestMotive01Truck1Battery2LineItems() {
		List<ConfiguratorWS.QuoteLineItem> returnItems = new List<ConfiguratorWS.QuoteLineItem>{};

		returnItems.add(createJsonLineItem('66211-WBKC', 1, null, '66211-WBKC-12-E90-13', false, false, null, null, null, null));
		returnItems.add(createJsonLineItem('EI1-CM-2GE', 1, null, 'EI1-CM-2GE-208V', false, false, null, null, '208', '24V'));
		returnItems.add(createJsonLineItem('EI1-CM-2GE', 1, null, 'EI1-CM-2GE-220V', false, false, null, null, '220', '24V'));
		returnItems.add(createJsonLineItem('EI1-HL-2YE', 1, null, 'EI1-HL-2YE-480V', false, false, null, null, '480', '24V'));
		returnItems.add(createJsonLineItem('EI3-HL-4YE', 1, null, 'EI3-HL-4YE-480V', false, false, null, null, '480', '24V'));

		return returnItems;
	}

	private List<ConfiguratorWS.QuoteLineItem> createJsonTestMotive01Truck2Battery1LineItems() {
		List<ConfiguratorWS.QuoteLineItem> returnItems = new List<ConfiguratorWS.QuoteLineItem>{};

		returnItems.add(createJsonLineItem('503744S-WGRN', 1, null, '503744S-WGRN-18-E155-15', false, false, null, null, null, null));
		returnItems.add(createJsonLineItem('EI3-IN-4CE', 1, null, 'EI3-IN-4CE-600V', false, false, null, null, '600', '24V'));
		returnItems.add(createJsonLineItem('EI3-IN-4YE', 1, null, 'EI3-IN-4YE-480V', false, false, null, null, '480', '24V'));
		returnItems.add(createJsonLineItem('EI3-LP-4GE', 1, null, 'EI3-LP-4GE-208V', false, false, null, null, '208', '24V'));
		returnItems.add(createJsonLineItem('EI3-LP-4GE', 1, null, 'EI3-LP-4GE-220V', false, false, null, null, '220', '24V'));
		returnItems.add(createJsonLineItem('EI3-NR-4GE', 1, null, 'EI3-NR-4GE-240V', false, false, null, null, '240', '24V'));

		return returnItems;
	}

	private List<ConfiguratorWS.QuoteLineItem> createJsonTestMotive01Truck2Battery2LineItems() {
		List<ConfiguratorWS.QuoteLineItem> returnItems = new List<ConfiguratorWS.QuoteLineItem>{};

		returnItems.add(createJsonLineItem('503744D-WGRN', 1, null, '503744D-WGRN-18-E125D-15', false, false, null, null, null, null));
		returnItems.add(createJsonLineItem('EI3-IN-4CE', 1, null, 'EI3-IN-4CE-600V', false, false, null, null, '600', '24V'));
		returnItems.add(createJsonLineItem('EI3-IN-4YE', 1, null, 'EI3-IN-4YE-480V', false, false, null, null, '480', '24V'));
		returnItems.add(createJsonLineItem('EI3-LP-4GE', 1, null, 'EI3-LP-4GE-208V', false, false, null, null, '208', '24V'));
		returnItems.add(createJsonLineItem('EI3-LP-4GE', 1, null, 'EI3-LP-4GE-220V', false, false, null, null, '220', '24V'));
		returnItems.add(createJsonLineItem('EI3-NR-4GE', 1, null, 'EI3-NR-4GE-240V', false, false, null, null, '240', '24V'));

		return returnItems;
	}

	private ConfiguratorWS.OEMSpecTruckSpecs createJsonTestMotive01Truck1Specs() {
		ConfiguratorWS.OEMSpecTruckSpecs ts = new ConfiguratorWS.OEMSpecTruckSpecs();
		ts.manufacturer = 'Crown';
		ts.modelOrClass = '20WB, 30WB, 40WB / 3';
		ts.typeAndCover = 'Electric Walkie / N';
		ts.dimensions = '31.75&quot; x 13.12&quot; x 23.25&quot;';
		ts.connector = '6325-SB175 Gray';
		ts.voltageAndTerm = '24V / A- 17&quot;';
		ts.minWeight = '975lbs';
		ts.notes = 'Notes: Dimensions are Max battery dimensions';
		return ts;
	}

	private ConfiguratorWS.OEMSpecTruckSpecs createJsonTestMotive01Truck2Specs() {
		ConfiguratorWS.OEMSpecTruckSpecs ts = new ConfiguratorWS.OEMSpecTruckSpecs();
		ts.manufacturer = 'Toyota';
		ts.modelOrClass = '8BNCU15/18/20 (19") / 2';
		ts.typeAndCover = 'Stand-Up Rider / N';
		ts.dimensions = '38.50" x 18.50" x 32.30"';
		ts.connector = '6320-SB350 Gray';
		ts.voltageAndTerm = '36V / B- 11"';
		ts.minWeight = '2300lbs';
		ts.notes = 'Notes: Max weight 2600lbs - Max 1085 A.H.';
		return ts;
	}

	private List<ConfiguratorWS.QuoteConfig> createJsonTestMotive02QuoteConfigs() {
		List<ConfiguratorWS.QuoteConfig> returnList = new List<ConfiguratorWS.QuoteConfig>{};

		ConfiguratorWS.QuoteConfig newQuote = new ConfiguratorWS.QuoteConfig();
		newQuote.configuratorName = 'OEMSpec';
		newQuote.sfQuoteNumber = MOTIVE_TEST_QUOTE_NAME;
		newQuote.truckSpecs = createJsonTestMotive01Truck1Specs();
		newQuote.lineItems = createJsonTestMotive02LineItems();
		returnList.add(newQuote);

		return returnList;
	}

	private List<ConfiguratorWS.QuoteLineItem> createJsonTestMotive02LineItems() {
		List<ConfiguratorWS.QuoteLineItem> returnItems = new List<ConfiguratorWS.QuoteLineItem>{};

		returnItems.add(createJsonLineItem('99999EB', 1, null, null, true, true, null, null, null, null));
		returnItems.add(createJsonLineItem('99999GBM', 1, null, null, true, true, null, null, null, null));

		return returnItems;
	}

	private List<ConfiguratorWS.QuoteConfig> createJsonTestReserve01QuoteConfigs() {
		List<ConfiguratorWS.QuoteConfig> returnList = new List<ConfiguratorWS.QuoteConfig>{};

		ConfiguratorWS.QuoteConfig newQuote = new ConfiguratorWS.QuoteConfig();
		newQuote.configuratorName = 'Core';
		newQuote.sfQuoteNumber = RESERVE_TEST_QUOTE_NAME;
		newQuote.lineItems = createJsonTestReserve01LineItems();
		returnList.add(newQuote);

		return returnList;
	}

	private List<ConfiguratorWS.QuoteLineItem> createJsonTestReserve01LineItems() {
		List<ConfiguratorWS.QuoteLineItem> returnItems = new List<ConfiguratorWS.QuoteLineItem>{};

		returnItems.add(createJsonLineItem('2DX-31', 600, null,null , true, false, 'Battery', null, null, null));
		returnItems.add(createJsonLineItem('UD2H2T216A', 25, null, null, false, false, null, null, null, null));
		returnItems.add(createJsonLineItem('UD2H3T180A', 0, null, null, false, false, null, null, null, null));
		returnItems.add(createJsonLineItem('827511', 425, null, null, false, false, null, null, null, null));
		returnItems.add(createJsonLineItem('881867', 25, null, null, false, false, null, null, null, null));

		return returnItems;
	}


	private ConfiguratorWS.QuoteLineItem createJsonLineItem(String partNumber, Integer quantity, Decimal specialPrice, String technicalSpec, Boolean confirmationRequired, Boolean approvalRequired, String itemType, String itemNotes, String chargerVacSelected, String chargerVoltageSelected) {
		ConfiguratorWS.QuoteLineItem newItem = new ConfiguratorWS.QuoteLineItem();
		newItem.partNumber=partNumber;
		newItem.quantity=quantity;
		if (specialPrice!=null) {
			newItem.specialPrice=specialPrice;
		}
		newItem.technicalSpecId=technicalSpec;
		newItem.confirmationRequired = confirmationRequired;
		newItem.approvalRequired = approvalRequired;
		newItem.itemType = itemType;
		newItem.itemNotes = itemNotes;
		newItem.chargerVacSelected = chargerVacSelected;
		newItem.chargerVoltageSelected = chargerVoltageSelected;
		return newItem;
	}

	private void checkForProductsInSF() {
		Map<Id, Product2> products = new Map<Id, Product2>([Select Name From Product2 Where Name IN: partNumbers]);
		mapPartNumberToProduct = new Map<String, Product2>{};
		for (Product2 p : products.values()) {
			mapPartNumberToProduct.put(p.Name, p);
		}

		for (String pn : partNumbers) {
			if (mapPartNumberToProduct.containsKey(pn)) {
				validProductIds.add(mapPartNumberToProduct.get(pn).Id);

			} else {
				System.debug(LoggingLevel.INFO, '*** ' + pn + ' - Part Number does not exist in SF');

			}
		}

		System.debug(LoggingLevel.INFO, '*** validProductIds.size():  ' + validProductIds.size());
	}

	private void getMotivePricebookId() {
		pricebookId = [Select Id from Pricebook2 Where Name = :MOTIVE_PRICEBOOK_NAME].Id;
	}

	private void getReservePricebookId() {
		pricebookId = [Select Id from Pricebook2 Where Name = :RESERVE_PRICEBOOK_NAME].Id;
	}

	private void checkForPricesInSF() {
		List<PricebookEntry> pbes = [Select Product2Id, Pricebook2Id From PricebookEntry Where Pricebook2Id =: pricebookId And Product2Id IN: validProductIds]; 
		Map<Id, PricebookEntry> mapProductIdToPbe = new Map<Id, PricebookEntry>{};
		for (PricebookEntry pbe : pbes) {
			mapProductIdToPbe.put(pbe.Product2Id, pbe);
		}

		for (Id pId : validProductIds) {
			if (mapProductIdToPbe.containsKey(pId)) {
				validProductIdsWithPrice.add(pId);

			} else {
				System.debug(LoggingLevel.INFO, '*** ' + pId + ' - Price does not exist in SF Pricebook');

			}
		}

		System.debug(LoggingLevel.INFO, '*** validProductIdsWithPrice.size():  ' + validProductIdsWithPrice.size());
	}

}