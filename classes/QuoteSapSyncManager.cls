public with sharing class QuoteSapSyncManager {

	public static final String SETTING_QUOTE_TRIGGER_ENABLED_NAME = 'EMEA Quote Trigger Enabled';

	private Boolean triggerEnabled;
	private Boolean triggerIsInsert;
	private Boolean triggerIsUpdate;
	private Boolean triggerIsDelete;
	private Boolean triggerIsUndelete;
	private Map<Id, SBQQ__Quote__c> triggerOldMap;
	private Map<Id, SBQQ__Quote__c> triggerNewMap;

	private SapChangeQuoteBso bsoChange;
	private SapCreateQuoteBso bsoCreate;
	private static Map<String,Integration_Settings__c> settings;

	public QuoteSapSyncManager(Boolean isInsert, Boolean isUpdate, Boolean isDelete, Boolean isUndelete, Map<Id, SBQQ__Quote__c> oldMap, Map<Id, SBQQ__Quote__c> newMap) {
		settings = CustomSettingManager.fetchIntegrationSettingsMapByName();
		triggerEnabled = settings.get(SETTING_QUOTE_TRIGGER_ENABLED_NAME).Boolean_Value__c;

		triggerIsInsert = isInsert;
		triggerIsUpdate = isUpdate;
		triggerIsDelete = isDelete;
		triggerIsUndelete = isUndelete;
		triggerOldMap = oldMap;
		triggerNewMap = filterToEmeaOnly(newMap);

	}

	private Map<Id, SBQQ__Quote__c> filterToEmeaOnly(Map<Id, SBQQ__Quote__c> quoteMap) {
		Map<Id, SBQQ__Quote__c> returnMap = new Map<Id, SBQQ__Quote__c>{};
		
        if(quoteMap != null) {
            for (Id qId : quoteMap.keySet()) {
                SBQQ__Quote__c q = quoteMap.get(qId);
                if (q.SBU__c!=null && q.SBU__c.contains('EMEA')) {
                    returnMap.put(qId, q);
    
                }
    
            }
        }

		return returnMap;
	}

	public void syncToSap() {
		Map<Id, SBQQ__Quote__c> quotesForChange = new Map<Id, SBQQ__Quote__c>{};
		Map<Id, SBQQ__Quote__c> quotesForCreate = new Map<Id, SBQQ__Quote__c>{};

		if (triggerEnabled && triggerNewMap.size()==1) {
			if (!System.isFuture() && !System.isBatch()) {
				if (triggerIsInsert) {
					quotesForCreate = triggerNewMap;

				} else if (triggerIsUpdate) {
					quotesForCreate = getChangedQuotesFromOldAndNewMaps();
					quotesForChange = getChangedQuotesFromOldAndNewMapsChangeBapiOnly();

				}

			}

			if (quotesForCreate.size() > 0) {
				Utils.logContents('Quotes For SAP Create (Trigger)', String.valueOf(quotesForCreate));

                SObject quoteSObj = (SObject)quotesForCreate.values()[0];
				bsoCreate = new SapCreateQuoteBso();
				bsoCreate.syncQuoteHeaderToSap(quoteSObj, true);

			}

			if (quotesForChange.size() > 0) {
				Utils.logContents('Quotes For SAP Change (Trigger)', String.valueOf(quotesForChange));

                SObject quoteSObjChange = (SObject)quotesForChange.values()[0];
				bsoChange = new SapChangeQuoteBso();
				bsoChange.syncQuoteHeaderToSap(quoteSObjChange, true);

			}

		}

	}

	private Map<Id, SBQQ__Quote__c> getChangedQuotesFromOldAndNewMaps() {
		Map<Id, SBQQ__Quote__c> returnMap = new Map<Id, SBQQ__Quote__c>{};
		for (Id quoteId : triggerNewMap.keySet()) {
			SBQQ__Quote__c oldQuote = triggerOldMap.get(quoteId);
			SBQQ__Quote__c newQuote = triggerNewMap.get(quoteId);
			if (
				newQuote.Currency_Code__c != oldQuote.Currency_Code__c
				|| newQuote.Customer_Requested_Date__c != oldQuote.Customer_Requested_Date__c
				|| newQuote.Incoterms__c != oldQuote.Incoterms__c
				|| newQuote.Incoterms2_Text__c != oldQuote.Incoterms2_Text__c
				|| newQuote.Payment_Terms_SAP__c != oldQuote.Payment_Terms_SAP__c
				|| newQuote.Reference_Information_Number__c != oldQuote.Reference_Information_Number__c
				|| newQuote.Sales_Area_Data__c != oldQuote.Sales_Area_Data__c
				|| newQuote.SBQQ__ExpirationDate__c != oldQuote.SBQQ__ExpirationDate__c
				//|| newQuote.Ship_To_Data__c != oldQuote.Ship_To_Data__c
				|| newQuote.Select_Ship_To_SAD__c != oldQuote.Select_Ship_To_SAD__c
				|| newQuote.Shipping_Conditions__c != oldQuote.Shipping_Conditions__c
				|| newQuote.Valid_From__c != oldQuote.Valid_From__c
				) {
				returnMap.put(quoteId, newQuote);

			}
		}
		return returnMap;
	}

	private Map<Id, SBQQ__Quote__c> getChangedQuotesFromOldAndNewMapsChangeBapiOnly() {
		Map<Id, SBQQ__Quote__c> returnMap = new Map<Id, SBQQ__Quote__c>{};
		for (Id quoteId : triggerNewMap.keySet()) {
			SBQQ__Quote__c oldQuote = triggerOldMap.get(quoteId);
			SBQQ__Quote__c newQuote = triggerNewMap.get(quoteId);
			if (
				newQuote.Ship_To_SAP_ID_Text__c != oldQuote.Ship_To_SAP_ID_Text__c 
				|| newQuote.Your_Contact_in_the_Office__c != oldQuote.Your_Contact_in_the_Office__c
				|| newQuote.Your_Sales_Contact__c != oldQuote.Your_Sales_Contact__c
				) {
				returnMap.put(quoteId, newQuote);

			}
		}
		return returnMap;
	}

}