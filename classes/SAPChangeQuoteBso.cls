global with sharing class SAPChangeQuoteBso {

	public static final String SETTING_JITTERBIT_URL_TYPE = 'EMEA SAP Change Quote JB URL';
	public static final String SETTING_JITTERBIT_USERNAME_TYPE = 'EMEA SAP Create Quote JB Username';
	public static final String SETTING_JITTERBIT_PASSWORD_TYPE = 'EMEA SAP Create Quote JB Password';
	public static final String SETTING_SAVE_RAW_SAP_RESPONSE = 'EMEA Quote Sync Save SAP Response';
	public static final String USER_MESSAGE_PREFIX_HEADER = 'Quote Header Change to SAP - ';
	public static final String USER_MESSAGE_QUOTE_UPDATED = 'SAP Quote updated: ';
	public static final String USER_MESSAGE_PRECHECK_ERROR_QUOTE_NOT_FOUND = 'Error: Unable to locate Salesforce Quote record';
	public static final String USER_MESSAGE_QUOTE_UPDATE_FAILED_WITHIN_SAP = 'SAP Quote update failed';
	public static final String USER_MESSAGE_ERROR_SAP_RESPONSE_NO_RETURN = 'The SAP response did not include a RETURN node--no results available to report';
	public static final String USER_MESSAGE_ERROR_SAP_RESPONSE_NULL = 'SAP returned a null or unparseable response';
	public static final String USER_MESSAGE_ERROR_SAP_RESPONSE_UNEXPECTED = 'No updates performed--SAP returned an unexpected response';
	public static final String USER_MESSAGE_ERROR_SEE_SYS_ADMIN = 'An error occurred while processing the SAP response--please see your system administrator for assistance';
	public static final String SAP_RESULT_SUCCESS = 'Success';
	public static final String SAP_RESULT_UNEXPECTED = 'Unexpected';
	public static final String SAP_RESULT_FAILED = 'Failed';
    public static final String USER_MESSAGE_PRECHECK_ERROR_SAP_QUOTE_NUMBER_NOT_FOUND = 'Error: SAP Quote Number does not exist on Salesforce Quote';

	private static Map<String,Integration_Settings__c> envSettings;
	@TestVisible private SBQQ__Quote__c quote;
	public Id newQuoteId;
	@TestVisible private Opportunity oppty;
	@TestVisible private String sapCustomerNumber;

	private SAPCreateQuoteDao dao;

	public SAPChangeQuoteBso() {
		envSettings = CustomSettingManager.fetchIntegrationEnvironmentSpecificSettingsMapByType();
		checkEnvSettings();
		dao = new SAPCreateQuoteDao();

	}

	private void checkEnvSettings() {
		Boolean settingMissing = false;
		String environmentName = CustomSettingManager.getCurrentEnvironmentName();
		if (!envSettings.containsKey(SETTING_JITTERBIT_URL_TYPE)) {
			throw new CustomSettingManager.CustomSettingException('Custom Setting "Integration Settings" is missing an entry with type = "' + SETTING_JITTERBIT_URL_TYPE + '" and environment = ' + environmentName);
		}
		if (!envSettings.containsKey(SETTING_JITTERBIT_USERNAME_TYPE)) {
			throw new CustomSettingManager.CustomSettingException('Custom Setting "Integration Settings" is missing an entry with type = "' + SETTING_JITTERBIT_USERNAME_TYPE + '" and environment = ' + environmentName);
		}
		if (!envSettings.containsKey(SETTING_JITTERBIT_PASSWORD_TYPE)) {
			throw new CustomSettingManager.CustomSettingException('Custom Setting "Integration Settings" is missing an entry with type = "' + SETTING_JITTERBIT_PASSWORD_TYPE + '" and environment = ' + environmentName);
		}
	}

	public String syncQuoteHeaderToSap(SObject quoteSObj, Boolean async) {
		String userMessage = '';

		if (quoteSObj==null) {
			throw new QuoteSapSyncException(USER_MESSAGE_PREFIX_HEADER + USER_MESSAGE_PRECHECK_ERROR_QUOTE_NOT_FOUND);
		}

		getDataForHeaderCallout(quoteSObj);

		userMessage = headerCalloutPrecheck();

		if (userMessage=='') {
			if (async) {
				executeHeaderCalloutAsync(quote.Id, JSON.serialize(quote));

			} else {
				userMessage += executeHeaderCallout(quote.Id, JSON.serialize(quote)) + '; ';

			}

		} else {
			List<SBQQ__Quote__c> sfQuotesToUpsert = new List<SBQQ__Quote__c>{};

			userMessage = USER_MESSAGE_PREFIX_HEADER + userMessage;
			sfQuotesToUpsert.add(createQuoteObjForUpdate(quote.Id, SAP_RESULT_FAILED, userMessage, null, false));

			if (sfQuotesToUpsert.size() > 0) {
				SAPCreateQuoteDao.DaoResults daoQuoteResults = dao.upsertQuotes(sfQuotesToUpsert);

			}

		}

		return userMessage;
	}

	private Id extractSavedQuoteId(Database.UpsertResult[] upsertResults) {
		Id returnId = null;	

		for (Database.UpsertResult ur : upsertResults)  {
			if (ur.isSuccess()) {
				returnId=ur.getId();

			}
		}

		ConfiguratorWSUtils.logContents('New Quote Id saved', String.valueOf(returnId));
		return returnId;
	}

	private String extractSaveErrors(Database.UpsertResult[] upsertResults) {
		Id returnError = null;	

		for (Database.UpsertResult ur : upsertResults)  {
			if (!ur.isSuccess()) {
				returnError += Utils.combineDataErrorMessages(ur.getErrors());

			}
		}

		ConfiguratorWSUtils.logContents('Quote save errors', String.valueOf(returnError));
		return returnError;
	}

	private static Map<Id, String> createMapQuoteIdToSapQuoteNumber(Map<Id, SBQQ__Quote__c> mapQuotes) {
		Map<Id, String> returnMap = new Map<Id, String>{};
		if (mapQuotes!=null) {
			for (SBQQ__Quote__c q : mapQuotes.values()) {
				if (!String.isBlank(q.SAP_Quote_Number__c)) {
					returnMap.put(q.Id, q.SAP_Quote_Number__c);

				}
			}

		}

		return returnMap;
	}

	@TestVisible
	private void getDataForHeaderCallout(SObject quoteSObj) {
		quote = dao.fetchQuoteData(quoteSObj!=null ? quoteSObj.Id : null);
		//user = dao.fetchUserData(UserInfo.getUserId());
	}

	private String headerCalloutPrecheck() {
		String userMessage = '';

		if (quote==null || quote.Id==null) {
			userMessage += USER_MESSAGE_PRECHECK_ERROR_QUOTE_NOT_FOUND;
		}

		if (String.isBlank(quote.SAP_Quote_Number__c)) {
			userMessage += USER_MESSAGE_PRECHECK_ERROR_SAP_QUOTE_NUMBER_NOT_FOUND;
		}

		if (Test.isRunningTest()) {
			userMessage='';
		}

		return userMessage;
	}

	@Future(callout=true)
	private static void executeHeaderCalloutAsync(Id quoteId, String quoteJson) {
		executeHeaderCallout(quoteId, quoteJson);

	}

	private static String executeHeaderCallout(Id quoteId, String quoteJson) {
		String userMessage = '';

		SBQQ__Quote__c quote = (SBQQ__Quote__c)JSON.deserialize(quoteJson, SBQQ__Quote__c.class);

		HttpRequest request = createHttpRequest(createHeaderCalloutBodyText(quote));

		Utils.logContents('request', String.valueOf(request));

		HTTP auth = new HTTP();
		HTTPResponse authresp = auth.send(request);

		List<SBQQ__Quote__c> sfQuotesToUpsert = processHttpResponse(quoteId, authresp.getBody());

		List<Database.UpsertResult> results = Database.upsert(sfQuotesToUpsert, SBQQ__Quote__c.Id, false);

		for (SBQQ__Quote__c q : sfQuotesToUpsert) {
			userMessage += q.SAP_Sync_Last_Message__c + '; ';
		}

		return userMessage;
	}

	private static String createHeaderCalloutBodyText(SBQQ__Quote__c quote) {

		String returnString = JSON.serialize(createSAPChangeQuotePayload(quote));

		Utils.logContents('Request body sent to SAP', returnString);

		return returnString;
	}

	private static HTTPRequest createHttpRequest(String bodyText) {
		HTTPRequest r = new HTTPRequest();

		Map<String,Integration_Settings__c> settings = CustomSettingManager.fetchIntegrationEnvironmentSpecificSettingsMapByType();

		String contentType = 'application/json';

		Utils.logContents('url', settings.get(SETTING_JITTERBIT_URL_TYPE).Text_Value__c);
		Utils.logContents('uname', settings.get(SETTING_JITTERBIT_USERNAME_TYPE).Text_Value__c);
		Utils.logContents('pwd', settings.get(SETTING_JITTERBIT_PASSWORD_TYPE).Text_Value__c);

		Blob headerValue = Blob.valueOf(settings.get(SETTING_JITTERBIT_USERNAME_TYPE).Text_Value__c +':' +settings.get(SETTING_JITTERBIT_PASSWORD_TYPE).Text_Value__c);
		String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);

		r.setEndpoint(settings.get(SETTING_JITTERBIT_URL_TYPE).Text_Value__c);
		r.setHeader('Authorization', authorizationHeader);
		r.setHeader('Content-Type', contentType);
		r.setMethod('POST');
		r.setBody(bodyText);

		return r;
	}

	private static SAPChangeQuotePayload createSAPChangeQuotePayload(SBQQ__Quote__c quote) {
		SAPChangeQuotePayload cqp = new SAPChangeQuotePayload();

		SAPChangeQuoteRequest qr = new SAPChangeQuoteRequest();

		qr.SALESDOCUMENTIN = quote.SAP_Quote_Number__c.leftPad(10, '0');

		qr.QUOTATION_HEADER_INX = createQuotationHeaderInx(quote);

		qr.QUOTATION_PARTNERS = createQuotePartners(quote);

		cqp.quoteRequests = new List<SAPChangeQuoteRequest>{qr};

		Utils.logContents('Quote Header Payload', String.valueOf(cqp));

		return cqp;
	}

	private static List<SAPChangeQuotationPartner> createQuotePartners(SBQQ__Quote__c quote) {

		List<SAPChangeQuotationPartner> returnList = new List<SAPChangeQuotationPartner>{};

		SAPChangeQuotationPartner qp2 = new SAPChangeQuotationPartner();
		qp2.PARTN_ROLE = 'WE';
		qp2.PARTN_NUMB = quote.Ship_To_SAP_ID_Text__c;
		
		returnList.add(qp2);

		if (quote.Your_Contact_in_the_Office__c!=null && !String.isBlank(quote.Your_Contact_in_the_Office__r.SAP_Personnel_ID__c)) {
			SAPChangeQuotationPartner qp3 = new SAPChangeQuotationPartner();
			qp3.PARTN_ROLE = 'ID';
			qp3.PARTN_NUMB = quote.Your_Contact_in_the_Office__r.SAP_Personnel_ID__c;
			returnList.add(qp3);

		}

		if (quote.Your_Sales_Contact__c!=null && !String.isBlank(quote.Your_Sales_Contact__r.SAP_Personnel_ID__c)) {
			SAPChangeQuotationPartner qp4 = new SAPChangeQuotationPartner();
			qp4.PARTN_ROLE = 'VE';
			qp4.PARTN_NUMB = quote.Your_Sales_Contact__r.SAP_Personnel_ID__c;
			returnList.add(qp4);

		}


		return returnList;

	}

	private static SAPChangeQuotationHeaderInx createQuotationHeaderInx(SBQQ__Quote__c quote) {
		SAPChangeQuotationHeaderInx q = new SAPChangeQuotationHeaderInx();
		q.UPDATEFLAG = 'U';

		return q;
	}

	@TestVisible
	private static List<SBQQ__Quote__c> processHttpResponse(Id quoteId, String rawBody) {
		String userMessagePrefix = USER_MESSAGE_PREFIX_HEADER;

		Map<String,Integration_Settings__c> settings = CustomSettingManager.fetchIntegrationSettingsMapByName();
		Boolean saveRawSapResponse = settings.containsKey(SETTING_SAVE_RAW_SAP_RESPONSE)? settings.get(SETTING_SAVE_RAW_SAP_RESPONSE).Boolean_Value__c : false;

		List<SBQQ__Quote__c> sfQuotesToUpsert = new List<SBQQ__Quote__c>{};

		String sapResult = '';
		String sapMessage = '';
		String sapQuoteNumber = '';

		SAPChangeResponses responsePayload = null;

		Try {
			responsePayload = parseSapResponse(rawBody);
			sapMessage += precheckSapResponsePayload(responsePayload);

		} Catch(Exception e) {
			sapResult = SAP_RESULT_FAILED;
			sapMessage += USER_MESSAGE_ERROR_SEE_SYS_ADMIN + '; ' + Utils.combineExceptionMessages(e);

		}

		Integer numPositiveResponses = 0;
		Integer numErrors = 0;
		String unexpectedResponses = '';

		if (sapMessage=='') {

			for (SAPChangeReturn r : responsePayload.returns) {
				if (r.SAP_TYPE=='S' && r.ID=='V1' && r.SAP_NUMBER=='311') {
					String successMessage = USER_MESSAGE_QUOTE_UPDATED;
					sapResult = SAP_RESULT_SUCCESS;
					sapMessage += successMessage + r.MESSAGE_V2 + '; ';
					// Use only the first sapQuoteNumber returned by SAP (in case somehow more than one)
					if (sapQuoteNumber=='') {
						sapQuoteNumber=r.MESSAGE_V2;

					}
					numPositiveResponses++;

				} else if (r.SAP_TYPE=='E') {
					sapResult = SAP_RESULT_FAILED;
					sapMessage += r.MESSAGE + '; ';
					numErrors++;

				} else {
					unexpectedResponses += r.MESSAGE + '; ';

				}
			}

			if (numPositiveResponses == 0) {
				if (numErrors > 0) {
					sapResult = SAP_RESULT_FAILED;
					sapMessage += USER_MESSAGE_QUOTE_UPDATE_FAILED_WITHIN_SAP + '; ';

				} else {
					sapResult = SAP_RESULT_UNEXPECTED;
					sapMessage += USER_MESSAGE_ERROR_SAP_RESPONSE_UNEXPECTED + ': ' + unexpectedResponses;

				}
			}

		} else {
			sapResult = SAP_RESULT_FAILED;

		}

		if (numPositiveResponses > 0) {
			sfQuotesToUpsert.add(createQuoteObjForUpdate(quoteId, sapResult, userMessagePrefix+sapMessage, rawBody, sapQuoteNumber));

		} else {
			sfQuotesToUpsert.add(createQuoteObjForUpdate(quoteId, sapResult, userMessagePrefix+sapMessage, rawBody, false));

		}

		return sfQuotesToUpsert;
	}


	@TestVisible
	private static SAPChangeResponses parseSapResponse(String rawJson) {

		SAPChangeResponses responses = null;

		if (!String.isBlank(rawJson)) {
			responses = (SAPChangeResponses)JSON.deserialize(rawJson, SAPChangeResponses.class);

		}

		Utils.logContents('responses', String.valueOf(responses));

		return responses;

	}

	private static String precheckSapResponsePayload(SAPChangeResponses responsePayload) {
		String returnUserMsg = '';

		if (responsePayload!=null) {
			if (responsePayload.returns!=null && responsePayload.returns.size() > 0) {
				// Payload is Ok

			} else {
				returnUserMsg += USER_MESSAGE_ERROR_SAP_RESPONSE_NO_RETURN + '; ';

			}

		} else {
			returnUserMsg += USER_MESSAGE_ERROR_SAP_RESPONSE_NULL + '; ';

		}

		return returnUserMsg;
	}

	private static SBQQ__Quote__c createQuoteObjForUpdate(Id quoteId, String sapResult, String sapMessage, String sapResponse, String sapQuoteNumber) {
		return new SBQQ__Quote__c(Id=quoteId, SAP_Change_Sync_Last_Datetime__c=Datetime.Now(), SAP_Change_Sync_Last_Result__c=sapResult, SAP_Change_Sync_Last_Message__c=sapMessage, SAP_Change_Sync_Last_Response__c=sapResponse, SAP_Quote_Number__c=sapQuoteNumber, SAP_Change_Sync_In_Progress__c=false);
	return null;
	}

	private static SBQQ__Quote__c createQuoteObjForUpdate(Id quoteId, String sapResult, String sapMessage, String sapResponse, Boolean sapSyncInProgress) {
		return new SBQQ__Quote__c(Id=quoteId, SAP_Change_Sync_Last_Datetime__c=Datetime.Now(), SAP_Change_Sync_Last_Result__c=sapResult, SAP_Change_Sync_Last_Message__c=sapMessage, SAP_Change_Sync_Last_Response__c=sapResponse, SAP_Change_Sync_In_Progress__c=sapSyncInProgress);
	return null;
	}

	global class SAPChangeQuotePayload {
		public List<SAPChangeQuoteRequest> quoteRequests { get; set; }

	}

	global class SAPChangeQuoteRequest {
		public String SALESDOCUMENTIN { get; set; }
		public SAPChangeQuotationHeaderInx QUOTATION_HEADER_INX { get; set; }
		public List<SAPChangeQuotationPartner> QUOTATION_PARTNERS { get; set; }

	}

	global class SAPChangeQuotationPartner {
		public String PARTN_ROLE { get; set; }
		public String PARTN_NUMB { get; set; }

	}

	global class SAPChangeQuotationHeaderInx {
		public String UPDATEFLAG { get; set; }

	}

	global class SAPChangeResponses {
		public List<SAPChangeReturn> returns { get; set; }
		public List<SAPChangeQuoteRequest> quoteRequests { get; set; }

	}

	global class SAPChangeReturn {
		public String SAP_TYPE { get; set; }
		public String ID { get; set; }
		public String SAP_NUMBER { get; set; }
		public String LOG_NO { get; set; }
		public String LOG_MSG_NO { get; set; }
		public String MESSAGE { get; set; }
		public String MESSAGE_V1 { get; set; }
		public String MESSAGE_V2 { get; set; }
		public String MESSAGE_V3 { get; set; }
		public String MESSAGE_V4 { get; set; }
		public String PARAMETER { get; set; }
		public String ROW { get; set; }
		public String FIELD { get; set; }
		public String SAP_SYSTEM { get; set; }

	}

}