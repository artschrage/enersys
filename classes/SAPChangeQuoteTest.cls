@isTest
private class SAPChangeQuoteTest
{
	static SAPChangeQuoteTestDataFactory testData;

	static void setup() {
		CustomSettingManager.fillCustomSettingsWithDefaults();
		//CustomSettingManager.fillIntegrationEnvironmentSpecificSettingsWithDefaults();
		testData = new SAPChangeQuoteTestDataFactory();
		setCurrentUserSbuToEmeaMP();

	}

	static void setCurrentUserSbuToEmeaMP() {
		User u = [Select SBU__c From User Where Id = :UserInfo.getUserId()];
		if (u.SBU__c!='MP - EMEA') {
			u.SBU__c = 'MP - EMEA';
			update u;

		}

	}

	@isTest
	static void testSAPChangeQuoteHeaderSuccess()
	{
		setup();
		testData.setupHeaderScenario1();

		Test.setMock(HttpCalloutMock.class, SAPCreateQuoteTestHttpCalloutMock.getStaticResourceMock_Ok());

		SBQQ__Quote__c testQuote = testData.quotes.values()[0];

		Test.startTest();

		//Map<Id, SBQQ__Quote__c> quotes = SAPChangeQuoteTestDataFactory.createGenericTestQuotesForOpptys(1, testData.opptys, testData.mapAcctIdToSalesAreaDatas, /*testData.mapSalesAreaDataIdToShipToData,*/ testData.mapSapCodeToId);
		testQuote.Ship_To_SAP_ID_Text__c = String.valueOf(Integer.valueOf(testQuote.Ship_To_SAP_ID_Text__c + 1));
		update testQuote;

		Test.stopTest();

		List<SBQQ__Quote__c> updatedQuotes = [Select SAP_Change_Sync_Last_Message__c From SBQQ__Quote__c Where Id = :testQuote.Id];

		Utils.logContents('SAP_Change_Sync_Last_Message__c', updatedQuotes[0].SAP_Change_Sync_Last_Message__c);

		System.assert(updatedQuotes[0].SAP_Change_Sync_Last_Message__c!=null, 'The Quote Trigger was not able to update Quote.SAP_Change_Sync_Last_Message__c for success scenario');
		System.assert(updatedQuotes[0].SAP_Change_Sync_Last_Message__c.contains(SAPChangeQuoteBso.USER_MESSAGE_QUOTE_UPDATED), 'The Quote Trigger did not properly update SAP_Change_Sync_Last_Message__c for success scenario');
	}

	@isTest
	static void testSAPChangeQuoteHeaderErrorSapInternal()
	{
		setup();
		testData.setupHeaderScenario1();

		Test.setMock(HttpCalloutMock.class, SAPCreateQuoteTestHttpCalloutMock.getStaticResourceMock_Error());

		SBQQ__Quote__c testQuote = testData.quotes.values()[0];

		Test.startTest();

		testQuote.Ship_To_SAP_ID_Text__c = String.valueOf(Integer.valueOf(testQuote.Ship_To_SAP_ID_Text__c + 1));
		update testQuote;

		Test.stopTest();

		List<SBQQ__Quote__c> updatedQuotes = [Select SAP_Change_Sync_Last_Message__c From SBQQ__Quote__c Where Id = :testData.quotes.values()[0].Id];

		System.assert(updatedQuotes[0].SAP_Change_Sync_Last_Message__c!=null, 'The Quote Trigger was not able to update Quote.SAP_Change_Sync_Last_Message__c for SAP internal error scenario');
		System.assert(updatedQuotes[0].SAP_Change_Sync_Last_Message__c.contains(SAPChangeQuoteBso.USER_MESSAGE_QUOTE_UPDATE_FAILED_WITHIN_SAP), 'The Quote Trigger did not properly update SAP_Change_Sync_Last_Message__c for SAP internal error scenario');
	}

	@isTest
	static void testSAPChangeQuoteHeaderErrorQuoteNotFound()
	{
		setup();
		testData.setupHeaderScenario1();

		Test.startTest();

		SAPChangeQuoteBso bso = new SAPChangeQuoteBso();

		String errorReturned = '';

		Try {
			bso.syncQuoteHeaderToSap(null, true);

		} Catch(Exception e) {
			errorReturned = e.getMessage();

		}

		Test.stopTest();

		System.assert(errorReturned.contains(SAPChangeQuoteBso.USER_MESSAGE_PRECHECK_ERROR_QUOTE_NOT_FOUND), 'The Quote Trigger did not properly return error Quote Not Found');
	}

	@isTest
	static void testSAPResponseNull()
	{
		setup();
		testData.setupHeaderScenario1();

		Test.startTest();

		List<SBQQ__Quote__c> quotesToUpsert = SAPChangeQuoteBso.processHttpResponse(null, null);

		Test.stopTest();

		String expectedUserMessage = SAPChangeQuoteBso.USER_MESSAGE_ERROR_SAP_RESPONSE_NULL;

		System.assert(quotesToUpsert[0].SAP_Change_Sync_Last_Message__c.contains(expectedUserMessage), 'The Quote Trigger did not properly handle a null response from SAP');

	}

	@isTest
	static void testSAPResponseUnexpected()
	{
		setup();
		testData.setupHeaderScenario1();

		String sapMessage = 'SAP did something unexpected';
		String sapUnexpectedResponse = '{"returns":[{"SAP_TYPE":"U","SAP_SYSTEM":null,"SAP_NUMBER":"311","ROW":null,"PARAMETER":null,"MESSAGE_V4":null,"MESSAGE_V3":null,"MESSAGE_V2":null,"MESSAGE_V1":null,"MESSAGE":"' + sapMessage + '","LOG_NO":null,"LOG_MSG_NO":null,"ID":"V1","FIELD":null}]}';

		Test.startTest();

		List<SBQQ__Quote__c> quotesToUpsert = SAPChangeQuoteBso.processHttpResponse(null, sapUnexpectedResponse);

		Test.stopTest();

		String expectedUserMessage = SAPChangeQuoteBso.USER_MESSAGE_ERROR_SAP_RESPONSE_UNEXPECTED;

		System.assert(quotesToUpsert[0].SAP_Change_Sync_Last_Message__c.contains(expectedUserMessage), 'The Quote Trigger did not properly handle an unexpected SAP response');
		System.assert(quotesToUpsert[0].SAP_Change_Sync_Last_Message__c.contains(sapMessage), 'The Quote Trigger did not properly save the unexpected SAP response on the Quote');
	}

	@isTest
	static void testSAPResponseNoReturns()
	{
		setup();
		testData.setupHeaderScenario1();

		String sapResponse = '{"returns":[]}';

		Test.startTest();

		List<SBQQ__Quote__c> quotesToUpsert = SAPChangeQuoteBso.processHttpResponse(null, sapResponse);

		Test.stopTest();

		String expectedUserMessage = SAPChangeQuoteBso.USER_MESSAGE_ERROR_SAP_RESPONSE_NO_RETURN;

		System.assert(quotesToUpsert[0].SAP_Change_Sync_Last_Message__c.contains(expectedUserMessage), 'The Quote Trigger did not properly handle an SAP response with no RETURN node');
	}

	@isTest
	static void testChangeMultipleSfQuoteHeadersNoCallout()
	{
		setup();
		testData.setupHeaderScenario1();

		Test.startTest();

		for (SBQQ__Quote__c q : testData.quotes.values()) {
			q.Ship_To_SAP_ID_Text__c = String.valueOf(Integer.valueOf(q.Ship_To_SAP_ID_Text__c + 1));
		}
		update testData.quotes.values();

		Test.stopTest();

		List<SBQQ__Quote__c> updatedQuotes = [Select SAP_Change_Sync_Last_Message__c From SBQQ__Quote__c Where Id = :testData.quotes.values()[0].Id];

		System.assert(updatedQuotes[0].SAP_Change_Sync_Last_Message__c==null, 'The Quote Trigger should not perform a callout when the Trigger batch contains more than 1 record');

	}

	@isTest
	static void testJsonParser() {
		setup();

		String jsonSampleSuccess = '{ "returns": [ { "SAP_TYPE": "S", "SAP_SYSTEM": "C01CLNT010", "SAP_NUMBER": "233", "ROW": "0", "PARAMETER": "SALES_HEADER_IN", "MESSAGE_V4": null, "MESSAGE_V3": null, "MESSAGE_V2": null, "MESSAGE_V1": "VBAKKOM", "MESSAGE": "SALES_HEADER_IN has been processed successfully", "LOG_NO": null, "LOG_MSG_NO": "000000", "ID": "V4", "FIELD": null }, { "SAP_TYPE": "W", "SAP_SYSTEM": "C01CLNT010", "SAP_NUMBER": "555", "ROW": "0", "PARAMETER": "VBUVKOM", "MESSAGE_V4": null, "MESSAGE_V3": null, "MESSAGE_V2": null, "MESSAGE_V1": "VBAKKOM", "MESSAGE": "The sales document is not yet complete: Edit data", "LOG_NO": null, "LOG_MSG_NO": "000000", "ID": "V1", "FIELD": null }, { "SAP_TYPE": "S", "SAP_SYSTEM": "C01CLNT010", "SAP_NUMBER": "311", "ROW": "0", "PARAMETER": "SALES_HEADER_IN", "MESSAGE_V4": null, "MESSAGE_V3": null, "MESSAGE_V2": "220000432", "MESSAGE_V1": "Quotation", "MESSAGE": "Quotation 220000432 has been saved", "LOG_NO": null, "LOG_MSG_NO": "000000", "ID": "V1", "FIELD": null } ] }';
		String jsonSampleFailed = '{ "returns": [ { "SAP_TYPE": "S", "SAP_SYSTEM": "C01CLNT010", "SAP_NUMBER": "233", "ROW": "0", "PARAMETER": "SALES_HEADER_IN", "MESSAGE_V4": null, "MESSAGE_V3": null, "MESSAGE_V2": null, "MESSAGE_V1": "VBAKKOM", "MESSAGE": "SALES_HEADER_IN has been processed successfully", "LOG_NO": null, "LOG_MSG_NO": "000000", "ID": "V4", "FIELD": null }, { "SAP_TYPE": "E", "SAP_SYSTEM": "C01CLNT010", "SAP_NUMBER": "331", "ROW": "1", "PARAMETER": "SALES_SCHEDULES_IN", "MESSAGE_V4": null, "MESSAGE_V3": null, "MESSAGE_V2": null, "MESSAGE_V1": "000010", "MESSAGE": "Item 000010 does not exist", "LOG_NO": null, "LOG_MSG_NO": "000000", "ID": "V1", "FIELD": null }, { "SAP_TYPE": "W", "SAP_SYSTEM": "C01CLNT010", "SAP_NUMBER": "555", "ROW": "0", "PARAMETER": "VBUVKOM", "MESSAGE_V4": null, "MESSAGE_V3": null, "MESSAGE_V2": null, "MESSAGE_V1": "000010", "MESSAGE": "The sales document is not yet complete: Edit data", "LOG_NO": null, "LOG_MSG_NO": "000000", "ID": "V1", "FIELD": null }, { "SAP_TYPE": "E", "SAP_SYSTEM": "C01CLNT010", "SAP_NUMBER": "219", "ROW": "0", "PARAMETER": null, "MESSAGE_V4": null, "MESSAGE_V3": null, "MESSAGE_V2": null, "MESSAGE_V1": null, "MESSAGE": "Sales document was not changed", "LOG_NO": null, "LOG_MSG_NO": "000000", "ID": "V4", "FIELD": null } ] }';

		Test.startTest();

		SAPChangeQuoteBso.SAPChangeResponses successResponses = SAPChangeQuoteBso.parseSapResponse(jsonSampleSuccess);
		SAPChangeQuoteBso.SAPChangeResponses failedResponses = SAPChangeQuoteBso.parseSapResponse(jsonSampleFailed);

		Test.stopTest();

		System.assert(successResponses.returns[0].SAP_TYPE == 'S', 'Salesforce did not properly parse sample success JSON');
		System.assert(failedResponses.returns[0].SAP_TYPE == 'S', 'Salesforce did not properly parse sample failed JSON');
	}

	@isTest
	static void outputJsonStructures()
	{
		SAPChangeQuoteBso.SAPChangeQuotePayload cqp = new SAPChangeQuoteBso.SAPChangeQuotePayload();

		SAPChangeQuoteBso.SAPChangeQuoteRequest qr = new SAPChangeQuoteBso.SAPChangeQuoteRequest();		

		SAPChangeQuoteBso.SAPChangeQuotationPartner qp = new SAPChangeQuoteBso.SAPChangeQuotationPartner();

		qr.QUOTATION_PARTNERS = new List<SAPChangeQuoteBso.SAPChangeQuotationPartner>{qp};
		qr.QUOTATION_HEADER_INX = new SAPChangeQuoteBso.SAPChangeQuotationHeaderInx();

		cqp.quoteRequests = new List<SAPChangeQuoteBso.SAPChangeQuoteRequest>{qr};

		SAPChangeQuoteBso.SAPChangeResponses cqr = new SAPChangeQuoteBso.SAPChangeResponses();		

		SAPChangeQuoteBso.SAPChangeReturn qsr = new SAPChangeQuoteBso.SAPChangeReturn();

		cqr.returns = new List<SAPChangeQuoteBso.SAPChangeReturn>{qsr};
		cqr.quoteRequests = new List<SAPChangeQuoteBso.SAPChangeQuoteRequest>{qr};

		Utils.logContents('SAPChangeQuotePayload', JSON.serialize(cqp));
		Utils.logContents('SAPChangeResponses', JSON.serialize(cqr));
	}

}