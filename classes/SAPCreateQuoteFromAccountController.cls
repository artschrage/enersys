public with sharing class SAPCreateQuoteFromAccountController {

	@TestVisible SAPCreateQuoteBso bso;
	@TestVisible SObject acctSObj;
	public String userMessage {get;set;}

	public SAPCreateQuoteFromAccountController(ApexPages.StandardController c) {
		bso = new SAPCreateQuoteBso();
		acctSObj=c.getRecord();
		
	}

	public void calloutToSap(){
		bso.createSapCustomerAndQuoteFromAccount(acctSObj);
	}
}