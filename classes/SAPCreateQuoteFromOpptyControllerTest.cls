@isTest
private class SAPCreateQuoteFromOpptyControllerTest
{
	@isTest
	static void testController()
	{
		CustomSettingManager.fillCustomSettingsWithDefaults();

		Test.startTest();

		SAPCreateQuoteFromOpptyController cqc = new SAPCreateQuoteFromOpptyController(new ApexPages.StandardController(new Opportunity()));

		String isOkToRedirect = cqc.isOkToRedirect();
		String launchUrl = cqc.createLaunchUrl();

		Test.stopTest();

		System.assert(cqc.bso!=null, 'The controller did not instatiate properly');
		System.assert(!String.isBlank(isOkToRedirect), 'The controller did not instatiate properly');
		System.assert(!String.isBlank(launchUrl), 'The controller did not instatiate properly');

	}
}