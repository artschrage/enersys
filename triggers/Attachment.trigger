trigger Attachment on Attachment (after insert, after update, after delete, after undelete) {
    if(Trigger.isAfter && (Trigger.isInsert || Trigger.isUpdate)) {
    	AttachmentTriggerHandler.processAfter(Trigger.isInsert, Trigger.isUpdate, Trigger.isDelete, Trigger.isUndelete, Trigger.oldMap, Trigger.newMap);
    }
}